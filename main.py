import ezodf
import re

from tkinter import filedialog
import customtkinter as ctk
import os
from pathlib import Path


    
class ImageViewer(ctk.CTk):
    WIDTH = 300
    HEIGHT = 150
    def __init__(self):
        super().__init__()
        
        self.input_filename = ""
        self.dirname = Path(".")
        self.check_dir = False

        self.init_GUI()
    
    def init_GUI(self):
        ctk.set_appearance_mode("dark")
        ctk.set_default_color_theme("blue")

        self.title("Email parser")
        self.geometry(f"{ImageViewer.WIDTH}x{ImageViewer.HEIGHT}")
        self.resizable(True, True)
        
        # Line 1
        self.open_btn = ctk.CTkButton(master=self, text="Open File", command=self.input_file)
        self.open_btn.grid(row=0, column=0, pady=5, padx=5, sticky="w")

        self.input_path_label = ctk.CTkLabel(master=self, text=self.input_filename)
        self.input_path_label.grid(row=0, column=1, pady=5, padx=5, sticky="w")

        # Line 2
        self.ok_button = ctk.CTkButton(master=self, text="Ok", command=self.ok_btn_callback)
        self.ok_button.grid(row=1, column=0, pady=5, padx=5, sticky="w")

        self.done_label = ctk.CTkLabel(master=self, text="")
        self.done_label.grid(row=1, column=1, pady=5, padx=5, sticky="w")

        # Line 3
        self.sort_label = ctk.CTkCheckBox(master=self, text="Sort emails")
        self.sort_label.grid(row=2, column=0, pady=5, padx=5, sticky="w")
        
        

    def input_file(self):
        self.input_filename = filedialog.askopenfilename(filetypes=[("ods files", "*.ods"),
                                                         ("All files", "*.*")])
        self.input_path_label.configure(text=self.input_filename)

    def output_dir(self):
        self.dirname = filedialog.askdirectory()
        self.output_path_label.configure(text=self.dirname)
        self.check_dir = True
    
    def create_dir(self):
        if not self.check_dir:
            isExist = os.path.exists(self.dirname)
            if not isExist:
                os.makedirs(self.dirname)

        
    def ok_btn_callback(self):
        self.start_parsing()
        
    def start_parsing(self):
        output = dict()

        doc = ezodf.opendoc(self.input_filename)

        for sheet in doc.sheets:
            output[sheet.name] = set()
            for row in sheet.rows():
                for row_cell in row:
                    if row_cell:
                        cell = str(row_cell.value).strip().lower()
                        emails = re.findall(r"([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)", cell)
                        if(len(emails) > 0):
                            for em in emails:
                                output[sheet.name].add(em)
        
    
        for sh in output:
            if self.sort_label.get():
                output[sh] = sorted(list(output[sh])) 
            else:
                output[sh] = list(output[sh])
            
        
        ods = ezodf.newdoc(doctype='ods', filename='emails.ods')
        for em in output:
            sheet = ezodf.Sheet(name=em, size=(len(output[em]), 1))
            ods.sheets += sheet
            for e in output[em]:
                sheet[output[em].index(e), 0].set_value(e)

            ods.save()
            self.done_label.configure(text="Done") 

    
app = ImageViewer()
app.mainloop()
import ezodf
import re



output = dict()

doc = ezodf.opendoc("data2.ods")

for sheet in doc.sheets:
    output[sheet.name] = []
    for row in sheet.rows():
        for row_cell in row:
            if row_cell:
                cell = str(row_cell.value).strip().lower()
                emails = re.findall(r"([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)", cell)
                for em in emails:
                    output[sheet.name].append(em)


# for sh in output:
#     output[sh] = sorted(list(output[sh]))                        

ods = ezodf.newdoc(doctype='ods', filename='emails.ods')
for em in output:
    sheet = ezodf.Sheet(name=em, size=(len(output[em]), 1))
    ods.sheets += sheet
    for e in output[em]:
        sheet[output[em].index(e), 0].set_value(e)

    ods.save()
